//
//  LSCoreTextSwift.h
//  LSCoreTextSwift
//
//  Created by mac on 2020/2/14.
//  Copyright © 2020 Max. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for LSCoreTextSwift.
FOUNDATION_EXPORT double LSCoreTextSwiftVersionNumber;

//! Project version string for LSCoreTextSwift.
FOUNDATION_EXPORT const unsigned char LSCoreTextSwiftVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LSCoreTextSwift/PublicHeader.h>


#import <LSCoreText/LSCoreText.h>
